# EAs.LiT Vocabulary and Shapes

This repository contains RDF vocabularies and Shacl shapes for EAs.LiT Items and Learning Outcomes, used in other services.

* Definition of concepts and properties for E-Assessment
* Shacl-Shapes, that show how concepts and properties are intended to be used
* Predefined data that may be used in conjunction with concepts, properties and shapes

Everything is defined in RDF Turtle syntax and was created manually, not using any tools.

## Note

Vocabularies might be outdated and not in a consistent state with the EAs.LiT implementation
